all: dendrite.yaml.template nginx.conf.template dendrite-server-autoconfig.sh

install: all
	install -D --mode=444 -t "$(PREFIX)"/opt/dendrite-server dendrite.yaml.template nginx.conf.template
	install --mode=666 -t "$(PREFIX)"/opt/dendrite-server dendrite-server-autoconfig.sh

dendrite-server-autoconfig.sh: dendrite-server-autoconfig.sh.template
	m4 --prefix-builtins -D__prefix__="$(PREFIX)" "$<" > "$@"

dendrite.yaml.template: dendrite.yaml.pretemplate
	m4 --prefix-builtins -D__prefix__="$(PREFIX)" "$<" > "$@"

nginx.conf.template: nginx.conf.pretemplate
	m4 --prefix-builtins -D__prefix__="$(PREFIX)" "$<" > "$@"
